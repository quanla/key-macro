var fs = require("fs");
var chokidar = require("chokidar");

var gulp = require("gulp");


function createStylusCompiler() {
    return require("./build/stylus-compiler").createCompiler({
        container: {
            dir: `src/style`,
            file: "style.styl",
        },
        lookupDirs: [`src`],
        distDir: `dist/css`,
    });
};

gulp.task("compile-stylus:watch", createStylusCompiler().watch);

