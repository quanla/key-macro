import {Config} from "./ui/config";
var Actions = require("./actions/actions.js").Actions;
// window.ReactDOM = ReactDOM;

// Show dev tools
nw.Window.get().showDevTools();

ReactDOM.render((
    <div>
        <Config/>
    </div>
), document.getElementById("app-container"));

Actions.registerActions();

console.log("App loaded successfully");