const Path = require("path");
var JsContent = require("./js-content/js-content.js").JsContent;
var projectsState = require("../ui/projects-state.js").projectsState;
var ClipboardUtil = require("./clipboard-util.js").ClipboardUtil;
var ChangeCasing = require("./change-casing/change-casing.js").ChangeCasing;
const {WebStorm} = require("./webstorm/webstorm.js");
const fs = rn("fs");

exports.comx = () => {
    WebStorm.getPath().then((path) => {
        let name = ChangeCasing.changeCase(Path.basename(path).replace(/\.[^.]+$/,""), ChangeCasing.DASH, ChangeCasing.CAMEL);

        let project = projectsState.getProject(path);

        let indexer = project.indexer;

        let parentComp = project.parentReactComponent || "GmComponent";

        let fileByExport = indexer.getFileByExport(parentComp);

        let imports = JsContent.addImport(path, parentComp, fileByExport, []);

        let str = `
            ${JsContent.serializeImports(imports, true)}
            import classnames from "classnames";

            export class ${name} extends ${parentComp} {
                render() {
                    return (
                        ${'<'}div>
                            ${name}
                        ${'</'}div>
                    );
                }
            }
        `.trim();
        ClipboardUtil.typeString(str);
    });
};