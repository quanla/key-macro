const robotjs = rn("robotjs");

function wsSelectMore() {
    robotjs.keyTap("w", ["alt"]);
}

function wsCopyPath() {
    robotjs.keyTap("c", ["command", "shift"]);
}

function copy() {
    robotjs.keyTap("c", ["command"]);
}
function cut() {
    robotjs.keyTap("x", ["command"]);
}

function left() {
    robotjs.keyTap("left");
}

function shiftDocHome() {
    robotjs.keyTap("home", ["shift", "command"]);
}

function wsCursorBack() {
    robotjs.keyTap("[", ["command"]);
}

function paste() {
    robotjs.keyTap("v", ["command"]);
}

const Robot = {
    wsSelectMore,
    left,
    copy,
    cut,
    shiftDocHome,
    wsCopyPath,
    wsCursorBack,
    paste,
};

exports.Robot = Robot;
