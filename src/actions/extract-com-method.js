var Robot = require("./robot/robot.js").Robot;
var ClipboardUtil = require("./clipboard-util.js").ClipboardUtil;
const robotjs = rn("robotjs");

exports.extractComMethod = () => {
    ClipboardUtil.copySelection().then((methodName) => {
        Robot.wsSelectMore();

        ClipboardUtil.cutSelection().then((methodContent) => {

            ClipboardUtil.typeString(methodName)
                .then(() => {
                    Robot.wsSelectMore();
                    Robot.wsSelectMore();
                    Robot.wsSelectMore();
                    robotjs.keyTap("left");
                    robotjs.keyTap("up");
                    ClipboardUtil.typeString(`\nfunction ${methodContent.replace(/^\s*/,"")}\n`);
                })
            ;

        });

    });
};