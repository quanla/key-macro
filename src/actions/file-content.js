
function getLineContent(lineNo, content) {
    lineNo = +lineNo;
    // console.log(content);
    return content.split(/\r?\n/g)[lineNo-1];
}

const FileContent = {
    getLineContent,
};

exports.FileContent = FileContent;