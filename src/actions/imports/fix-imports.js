const {ClipboardUtil} = require("../clipboard-util.js");
const {projectsState} = require("../../ui/projects-state.js");
const {WebStorm} = require("../webstorm/webstorm.js");
const {JsContent} = require("../js-content/js-content.js");
var clipboard = nw.Clipboard.get();
const robotjs = rn("robotjs");

const Path = require("path");
var Robot = require("../robot/robot.js").Robot;
const {Cols} = require("../../common/utils/common-utils.js");
const {ObjectUtil} = require("../../common/utils/common-utils.js");
const fs = rn("fs");

function exists(base, path) {
    let hasExt = path.endsWith(".js") || path.endsWith(".jsx");
    if (hasExt) {
        return fs.existsSync(Path.join(Path.dirname(base), path));
    } else {
        return fs.existsSync(Path.join(Path.dirname(base), path + ".js")) || fs.existsSync(Path.join(Path.dirname(base), path + ".jsx"));
    }
}


function copyBackToBeginning() {
    return new Promise((resolve, reject) => {

        robotjs.keyTap("left");
        Robot.shiftDocHome();
        Robot.copy();

        setTimeout(() => {
            resolve(clipboard.get("text"));
        }, 51);
    });
}

function getInfo() {
    return copyBackToBeginning().then((content) => {
        return WebStorm.getPath().then((path) => {
            return ({path, content});
        });
    });
}

exports.fixImport = () => {
    getInfo().then(({path, content}) => {
        let project = projectsState.getProject(path);
        let indexer = project.indexer;

        let imports = JsContent.getImports(content);
        let newImports = imports;
        imports.forEach((im) => {

            // im.from
            if (im.from.indexOf("/") > -1 && !exists(path, im.from)) {
                let newPath = indexer.getFileByName(Path.basename(im.from).replace(/\..+$/, ""));
                // console.log("----");
                // console.log(im.from);
                // console.log(newPath);
                if (newPath != null) {
                    // console.log(JsContent.getRelativePath(path, newPath));
                    newImports = Cols.replace1(newImports, im, ObjectUtil.update(im, {from: JsContent.getRelativePath(path, newPath)}));
                }
            }
        });
        ClipboardUtil.typeString(JsContent.applyImports(newImports, content, imports, path));
    });
};