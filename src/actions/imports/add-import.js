var Robot = require("../robot/robot.js").Robot;
const {ClipboardUtil} = require("../clipboard-util.js");
const {projectsState} = require("../../ui/projects-state.js");
const {WebStorm} = require("../webstorm/webstorm.js");
const {JsContent} = require("../js-content/js-content.js");
var clipboard = nw.Clipboard.get();


function getWord() {
    return new Promise((resolve, reject) => {
        Robot.wsSelectMore();

        Robot.copy();

        setTimeout(() => {
            resolve(clipboard.get("text"));
        }, 51);
    });
}

function copyBackToBeginning() {
    return new Promise((resolve, reject) => {

        Robot.left();

        Robot.shiftDocHome();

        Robot.copy();

        setTimeout(() => {
            resolve(clipboard.get("text"));
        }, 51);
    });
}

function getInfo() {
    return getWord().then((word) =>
        copyBackToBeginning().then((content) =>
            WebStorm.getPath().then((path) => ({word, path, content}))
        )
    );
}

exports.addImport = () => {
    getInfo().then(({word, path, content}) => {
        let project = projectsState.getProject(path);

        let indexer = project.indexer;

        let imports = JsContent.getImports(content);
        let newImports = imports;

        let name = word;

        let imported = JsContent.isImported(name, newImports);
        let fileByExport = indexer.getFileByExport(name);
        // console.log(`[${name}]: imported: ${imported}, fileByExport: ${fileByExport}`);
        if (!imported && fileByExport) {
            // console.log(`Adding ${name}`);
            newImports = JsContent.addImport(path, name, fileByExport, newImports);
            ClipboardUtil.typeString(JsContent.applyImports(newImports, content, imports, path));
        } else {
            // robotjs.keyTap("right");
            Robot.wsCursorBack();

        }
    });


};