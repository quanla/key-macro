const parsers = [
    { ptn: () => /import \{([\w, ]+)\} from ["']([-\w\/.]+)["'];/, extract: (m) => ({
        importeds: m[1].split(/\s*,\s*/g).map((key) => ({key, as: key})),
        from: m[2],
    })},
    { ptn: () => /(?:const|var|let) \{([\w, ]+)\} = require\(["']([-\w\/.]+)["']\);/, extract: (m) => ({
        importeds: m[1].split(/\s*,\s*/g).map((key) => ({key, as: key})),
        from: m[2],
    })},
    { ptn: () => /(?:const|var|let) (\w+) = require\(["']([-\w\/.]+)["']\);/, extract: (m) => ({
        importeds: [{key: null, as: m[1]}],
        from: m[2],
    })},
    { ptn: () => /import \* as (\w+) from ["']([-\w\/.]+)["'];/, extract: (m) => ({
        importeds: [{key: null, as: m[1]}],
        from: m[2],
    })},
    { ptn: () => /import (\w+) from ["']([-\w\/.]+)["'];/, extract: (m) => ({
        importeds: [{key: null, as: m[1]}],
        from: m[2],
    })},
    { ptn: () => /(?:const|var|let) (\w+) = require\(["']([-\w\/.]+)(?:\.jsx?)?["']\)\.(\w+);/, extract: (m) => {
        if (m[3] != m[1]) {
            throw "320253785239";
        }

        return {
            importeds: [{key: m[1], as: m[1]}],
            from: m[2],
        };
    }},
];

function readLines(content, fn) {
    let regex = /\r?\n/g;
    let index = 0;

    for (let m;(m=regex.exec(content)) != null;) {
        let interrupt = fn(content.substring(index, m.index), index);
        if (interrupt) {
            return;
        }
        index = m.index + m[0].length;
    }
    fn(content.substring(index), index);
}

const JsImportParser = {
    getImports(content) {
        let imports = [];
        readLines(content, (line, start) => {
            line = line.trim();
            if (line.length == 0 || line.startsWith("//")) {
                return;
            }
            for (let i = 0; i < parsers.length; i++) {
                let parser = parsers[i];
                let m = parser.ptn().exec(line);
                if (m != null) {
                    let im = parser.extract(m);
                    im.pos = {
                        from: start,
                        to: start + m[0].length,
                    };
                    imports.push(im);
                    return;
                }
            }
            // No match found
            return true;
        });
        return imports;
    }
};

exports.JsImportParser = JsImportParser;