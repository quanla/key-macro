const fs = require("fs");
var JsImportParser = require("./js-import-parser.js").JsImportParser;

fs.readFile(__dirname + "/js-import-parser.js", "utf8", (err, content) => {
    JsImportParser.getImports(content);
});