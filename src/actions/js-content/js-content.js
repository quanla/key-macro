var RegexUtil = require("../../common/utils/regex-util.js").RegexUtil;
const Path = require("path");
var JsImportParser = require("./js-import-parser.js").JsImportParser;
var ObjectUtil = require("../../common/utils/object-util.js").ObjectUtil;
var Cols = require("../../common/utils/cols.js").Cols;


function isImported(key, imports) {
    return !!imports.find((im) => im.importeds.find((impted) => impted.key == key));
}

// console.log(Path.relative(Path.dirname("/ui/sample.js"), "/utils/common.js"));

function getImportByFrom(from, imports) {
    return imports.find((im) => im.from == from);
}

function getRelativePath(from, to) {

    let relative = Path.relative(Path.dirname(from), to);

    return relative.replace(/\.jsx?$/, "");
}

function addImport(path, name, fromFile, imports) {
    let relative = getRelativePath(path, fromFile);

    let im = getImportByFrom(relative, imports);
    if (im == null) {
        return imports.concat([
            {
                importeds: [{key: name, as: name}],
                from: relative,
            }
        ]);
    } else {
        return Cols.replace1(imports, im, ObjectUtil.update(im, {importeds: im.importeds.concat({key: name, as: name})}));
    }

}

function serializeImports(imports, jsx) {
    return imports
        .map((im) => {
            let keys1 = im.importeds.filter((ited) => ited.key == null);
            let keys2 = im.importeds.filter((ited) => ited.key != null);

            let keys = keys1.length == 0 ? "" : keys1[0].as;
            keys += keys2.length == 0 ? "" : `${keys.length == 0 ? "" : ", "}{${keys2.map((ited) => ited.as).join(", ")}}`;
            return jsx ?
                `import ${keys} from "${im.from}";` :
                `const ${keys} = require("${im.from}");`
            ;
        })
        .join("\n")
        ;
}

function applyImports(imports, content, oldImports, path) {

    let lastImport = Cols.last(oldImports);
    return serializeImports(imports, path.endsWith(".jsx")) + content.substring(lastImport ? lastImport.pos.to : 0);
}

const JsContent = {
    getImports(content) {
        return JsImportParser.getImports(content);
    },
    serializeImports,
    isImported,
    addImport,
    applyImports,
    getRelativePath,
};

exports.JsContent = JsContent;