const CAMEL = {
    parse(text) {
        let tokens = [];
        let regExp = new RegExp("[A-Z][a-z0-9]*", "g");

        for (let m;(m=regExp.exec(text)) != null;) {
            tokens.push(m[0].toLowerCase());
        }
        return tokens;
    },
    format(tokens) {
        return tokens.map((t) => t[0].toUpperCase() + t.substring(1)).join("");
    },
};
const DASH = {
    parse(text) {
        let tokens = [];
        let regExp = new RegExp("[^-]+", "g");

        for (let m;(m=regExp.exec(text)) != null;) {
            tokens.push(m[0].toLowerCase());
        }
        return tokens;
    },
    format(tokens) {
        return tokens.join("-");
    },
};

function changeCase(text, fromType, toType) {
    let tokens = fromType.parse(text);
    return toType.format(tokens);
}

const ChangeCasing = {
    changeCase,
    DASH,
    CAMEL,
};

exports.ChangeCasing = ChangeCasing;