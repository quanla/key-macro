var Robot = require("../robot/robot.js").Robot;
const fs = rn("fs");
const robotjs = rn("robotjs");
var clipboard = nw.Clipboard.get();

function getPath() {
    return new Promise((resolve, reject) => {

        Robot.wsCopyPath();

        setTimeout(() => {
            let path = clipboard.get("text");

            resolve(path);
        }, 51);
    });
}


const WebStorm = {
    getPath,
};

exports.WebStorm = WebStorm;