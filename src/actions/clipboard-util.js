var Robot = require("./robot/robot.js").Robot;
const robotjs = rn("robotjs");
var clipboard = nw.Clipboard.get();

function typeString(str) {
    clipboard.set(str, "text");
    return new Promise((resolve, reject) => {
        console.log(4);
        setTimeout(() => {
            console.log(5);
            Robot.paste()
            resolve();
            console.log(6);
        }, 51);
    });
}

function copySelection() {
    return new Promise((resolve, reject) => {
        Robot.copy();
        setTimeout(() => {
            resolve(clipboard.get("text"));
        }, 51);
    });
}
function cutSelection() {
    return new Promise((resolve, reject) => {
        Robot.cut();
        setTimeout(() => {
            resolve(clipboard.get("text"));
        }, 51);
    });
}

const ClipboardUtil = {
    typeString,
    copySelection,
    cutSelection,
};

exports.ClipboardUtil = ClipboardUtil;