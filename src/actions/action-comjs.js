const Path = require("path");
var ClipboardUtil = require("./clipboard-util.js").ClipboardUtil;
var ChangeCasing = require("./change-casing/change-casing.js").ChangeCasing;
const {WebStorm} = require("./webstorm/webstorm.js");
const fs = rn("fs");

exports.comjs = () => {
    WebStorm.getPath().then((path) => {
        let name = ChangeCasing.changeCase(Path.basename(path).replace(/\.[^.]+$/,""), ChangeCasing.DASH, ChangeCasing.CAMEL);

        let str = `
            const ${name} = {
                create${name}() {
                    
                }
            };
            
            exports.${name} = ${name};
        `;
        ClipboardUtil.typeString(str);
    });
};