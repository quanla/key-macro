
import {ChangeCasing} from "./change-casing/change-casing";
import {addImport} from "./imports/add-import.js";
import {fixImport} from "./imports/fix-imports.js";
import {comjs} from "./action-comjs";
import {comx} from "./action-comx";
import {gitLogin} from "./git-login";
import {extractComMethod} from "./extract-com-method";
import {NwKeySequence} from "../common/nw-key-sequence/nw-key-sequence.js";

const Actions = {
    registerActions() {

        NwKeySequence.registerKeySequence("ctrl+X I I", () => {
            addImport();
        });
        NwKeySequence.registerKeySequence("ctrl+X I F", () => {
            fixImport();
        });
        NwKeySequence.registerKeySequence("ctrl+X C O M J S", () => {
            comjs();
        });
        NwKeySequence.registerKeySequence("ctrl+X C O M X", () => {
            comx();
        });
        NwKeySequence.registerKeySequence("ctrl+X L", () => {
            gitLogin();
        });
        NwKeySequence.registerKeySequence("ctrl+X C E", () => {
            extractComMethod();
        });
        NwKeySequence.registerKeySequence("ctrl+X C D C", () => {
            var clipboard = nw.Clipboard.get();
            let text = clipboard.get("text");
            text = text.replace(/\..+/,"");
            clipboard.set(ChangeCasing.changeCase(text, ChangeCasing.DASH, ChangeCasing.CAMEL), 'text');
        });
    }
};

exports.Actions = Actions;