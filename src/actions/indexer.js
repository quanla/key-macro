const glob = rn("glob");
const Path = require("path");
var RegexUtil = require("../common/utils/regex-util.js").RegexUtil;
var FileIndex = require("../common/file-index.js").FileIndex;

const Indexer = {
    createIndexer(path, {ignored}) {

        let exportsIndex = FileIndex.createFileIndex((content) => {
            return RegexUtil.getMatches(1, /exports\.(\w+)/g, content)
                .concat(RegexUtil.getMatches(1, /export const (\w+)/g, content))
                .concat(RegexUtil.getMatches(1, /export class (\w+)/g, content))
                .concat(RegexUtil.getMatches(1, /export function (\w+)/g, content))
                ;
        });

        let fileNameIndex = {};

        [Path.join(path, "**/*.js"), Path.join(path, "**/*.jsx")].forEach((path) =>
            glob(path, {ignore: ignored}, (err, files) => {
                files.forEach((file) => exportsIndex.addFileToIndex(file));
                fileNameIndex[Path.basename(path).replace(/\..+$/, "")] = path;
            })
        );

        // setTimeout(() => {
        //     exportsIndex.debug();
        //     // console.log(fileNameIndex);
        // }, 1000);

        return {
            getFileByExport(name) {
                // console.log(path);
                return exportsIndex.getFile(name);
            },
            getFileByName(name) {
                return fileNameIndex[name];
            },
        };
    },
};

exports.Indexer = Indexer;