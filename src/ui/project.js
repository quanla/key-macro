const {Indexer} = require("../actions/indexer");
const Projects = {
    createProject(p) {
        // console.log(path);
        let indexer = Indexer.createIndexer(p.path, {ignored: ["**/node_modules/**", `**/dist/**`, `**/assets/vendor/**`]});
        return {
            ...p,
            indexer,
        };
    }
};

exports.Projects = Projects;