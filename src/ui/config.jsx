import classnames from "classnames";
import {Storage} from "../common/storage";
import {GmComponent} from "../common/gm-component";
import {Projects} from "./project";
import {projectsState} from "./projects-state";

const store = Storage.createStorage("config");

export class Config extends GmComponent {

    constructor(props, context) {
        super(props, context);

        this.state = {
            projects: store.get("projects") || require("./projects-config")
        };
        projectsState.setProjects(this.state.projects.map((p) => Projects.createProject(p)));
    }

    set(key, value) {
        this.setState({[key]: value});
        store.set(key, value);
    }

    render() {
        const {projectPath} = this.state;
        return (
            <div className="">
                <input
                    className="form-control"
                    value={projectPath || ""}
                    onChange={(e) => this.set("projectPath", e.target.value)}
                />
            </div>
        );
    }
}