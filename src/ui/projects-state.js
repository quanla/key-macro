
let projects = null;

const projectsState = {
    setProjects(projects1) {
        projects = projects1;
    },
    getProject(path) {
        return projects.find((p) => path.startsWith(p.path));
    }
};

exports.projectsState = projectsState;