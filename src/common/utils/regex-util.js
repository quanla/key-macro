const RegexUtil = {
    getMatches(groupNo, regex, content) {
        let ret = [];
        for (let m;(m = regex.exec(content)) != null;) {
            ret.push(m[groupNo]);
        }
        return ret;
    },
    eachMatch(regex, content, fn) {
        for (let m;(m = regex.exec(content)) != null;) {
            fn(m);
        }
    }
};

exports.RegexUtil = RegexUtil;