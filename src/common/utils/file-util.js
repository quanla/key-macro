const fs = require("fs");
const path = require("path");

function recurse(dir, fn) {
    fs.readdirSync(dir).forEach((fileName) => {
        if (fs.lstatSync(path.join(dir, fileName)).isDirectory()) {
            recurse( path.join(dir, fileName), fn);
        }
        fn(dir, fileName);
    });
}

function recurseFiles(dir, fn) {

    fs.readdirSync(dir).forEach((fileName) => {
        if (fs.lstatSync(path.join(dir, fileName)).isDirectory()) {
            recurseFiles( path.join(dir, fileName), fn);
        } else {
            fn(dir, fileName);
        }
    });


}

const FileUtil = {
    recurse,
    recurseFiles,
};

exports.FileUtil = FileUtil;