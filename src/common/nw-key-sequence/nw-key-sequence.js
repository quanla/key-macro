var App = require("../app.js").App;
let listening = null;

App.onUnload(() => {
    if (listening) listening();
});

let W = "abcdefghijklmnopqrstuvwxyz0123456789";

function listenSequences(sequences) {
    let onUnloads = [];
    let childListening = null;

    for (let i = 0; i < W.length; i++) {
        let c = W[i];

        // console.log(`Registering ${c}`);
        let shortcut = new nw.Shortcut({
            key : c.toUpperCase(),
            active : function() {

                let matchSequences = sequences.filter((s) => s.sequence[0].toLowerCase() == c);

                onUnload();

                if (matchSequences.length == 0) {
                    //
                } else if (matchSequences[0].sequence.length == 1) {
                    matchSequences[0].action();
                } else {
                    let newSequences = matchSequences.map((s) => ({sequence: s.sequence.slice(1), action: s.action}));
                    childListening = listenSequences(newSequences);
                }
            },
            failed : function(msg) {
                // :(, fail to register the |key| or couldn't parse the |key|.
                console.log(msg);
            }
        });
        nw.App.registerGlobalHotKey(shortcut);

        onUnloads.push(() => nw.App.unregisterGlobalHotKey(shortcut));
    }
    let onUnload = () => {
        onUnloads.forEach((ou) => ou());
        onUnloads.length = 0;
        if (childListening) childListening();
    };
    return onUnload;
}

function getInitKeyStroke(keySequence) {
    let match = /^([^ ]+) (.+)$/.exec(keySequence);
    return {init: match[1], sequence: match[2].split(" ")};
}

let initKeyStrokes = {};

function assureInitKeyStroke(init) {
    let initKeyStroke = initKeyStrokes[init];
    if (initKeyStroke== null ) {
        let shortcut = new nw.Shortcut({
            key : init,
            active : function() {
                if (listening) listening();
                listening = listenSequences(initKeyStrokes[init]);
            },
            failed : function(msg) {
                // :(, fail to register the |key| or couldn't parse the |key|.
                console.log(msg);
            }
        });
        nw.App.registerGlobalHotKey(shortcut);

        App.onUnload(() => nw.App.unregisterGlobalHotKey(shortcut));
        initKeyStrokes[init] = [];
    }
}

const NwKeySequence = {
    registerKeySequence(keySequence, action) {
        let {init, sequence} = getInitKeyStroke(keySequence);

        assureInitKeyStroke(init);

        initKeyStrokes[init].push({sequence, action});
    }
};

exports.NwKeySequence = NwKeySequence;
