var Cols = require("./utils/cols.js").Cols;
let onUnloads = [];

window.onunload = () => {
    onUnloads.forEach((ou) => ou());
};

const App = {
    onUnload(listener) {
        onUnloads.push(listener);

        return () => Cols.remove1Mutate(onUnloads, listener);
    }

};

exports.App = App;