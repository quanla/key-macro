import {ObjectUtil} from "./utils/common-utils";

export class GmComponent extends React.PureComponent {
    onUnmounts = [];
    onMounts = [];

    componentDidMount() {
        this.mounted = true;
        this.onMounts.forEach((onMount)=> onMount());
    }

    componentWillUnmount() {
        this.mounted = false;
        this.onUnmounts.forEach((onUnmount)=> onUnmount());
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !ObjectUtil.isEqualsShallow(nextProps, this.props) || nextState != this.state;
    }

    safeUpdate() {
        if (this.mounted) {
            this.forceUpdate();
        }
    }

    onMount(f) {
        this.onMounts.push(f);
    }
    onUnmount(f) {
        this.onUnmounts.push(f);
    }

}