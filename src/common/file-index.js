var IndexUtil = require("./utils/index-util.js").IndexUtil;
var Cols = require("./utils/cols.js").Cols;
var ObjectUtil = require("./utils/object-util.js").ObjectUtil;
const fs = rn("fs");
const FileIndex = {
    createFileIndex(extract) {
        let index = {};

        let fileCleanups = {};

        function addFileToIndex(path) {
            fs.readFile(path, "utf8", (err, content) => {
                extract(content).forEach((key) => {
                    IndexUtil.addValToIndex(path, index, [key]);
                });
            });
            fileCleanups[path] = () => {

            };
        }

        return {
            addFileToIndex,
            removeFileFromIndex(path) {
                fileCleanups[path]();

                addFileToIndex(path);
            },
            getFile(key) {
                let list = index[key];
                return Cols.first(list);
            },
            size() {
                // console.log(ObjectUtil.keys(index));
                return ObjectUtil.keys(index).length;
            },
            debug() {
                let keys = ObjectUtil.keys(index);
                keys.sort(Cols.sortBy((str) => str.toLowerCase()));
                console.log(keys);
            }
        };
    }
};

exports.FileIndex = FileIndex;