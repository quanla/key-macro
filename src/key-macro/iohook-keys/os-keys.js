var Cols = require("../../common/utils/cols.js").Cols;
var KeyHook = require("./key-hook.js").KeyHook;
var KeyCombo = require("./key-combo.js").KeyCombo;

var MacKeyMap = require("./mac-key-map.js").MacKeyMap;

let keyHookEngine = KeyHook.createKeyHookEngine({
    translateEvent: (event) => MacKeyMap.fromKeyCode(event.keycode)
});

const osKeys = {
    on(keyPtn, fn) {

        let keyCombo = KeyCombo.compileCombo(keyPtn);
        keyHookEngine.onKeyDown((key, mods) => {
            // console.log(key, mods);
            // console.log(keyCombo);
            if (keyCombo.key == key && (keyCombo.mods == null ? 0 : keyCombo.mods.length) == mods.length
                && (mods.length == 0 || Cols.sameSet(keyCombo.mods, mods))
            ) {
                fn();
            }
        });
    }
};

exports.osKeys = osKeys;
