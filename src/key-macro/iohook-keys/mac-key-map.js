const keyboardLayout = [
    { startKeyCode: 1, keys: ["ESC"].concat("1234567890-=".split("")).concat(["BACKSPACE"])},
    { startKeyCode: 15, keys: ["TAB"].concat("qwertyuiop[]".toUpperCase().split("")).concat(["ENTER"])},
    { startKeyCode: 30, keys: "asdfghjkl;'".toUpperCase().split("")},
    { startKeyCode: 42, keys: ["SHIFT", "ZZZ"].concat("zxcvbnm,./".toUpperCase().split("")).concat(["SHIFT"])},
];

const specialKeys = {
    56: "ALT",
    3675: "COMMAND",
    3676: "COMMAND",
    29: "CTRL",
    57: "SPACE",
    57416: "UP",
    57424: "DOWN",
    57419: "LEFT",
    57421: "RIGHT",
};

function compileKeyboardLayout() {
    let keyCodeMap = {};

    keyboardLayout.forEach((line) => {
        line.keys.forEach((key, i) => {
            keyCodeMap[line.startKeyCode + i] = key;
        });
    });

    Object.assign(keyCodeMap, specialKeys);

    return keyCodeMap;
}

let keyCodeMap = compileKeyboardLayout();

const MacKeyMap = {
    fromKeyCode(keyCode) {
        return keyCodeMap[keyCode];
    }
};

exports.MacKeyMap = MacKeyMap;