var Cols = require("../../common/utils/common-utils.js").Cols;
const ioHook = require('iohook');


const modKeys = ["ALT", "CTRL", "COMMAND", "SHIFT"];

const KeyHook = {
    createKeyHookEngine({translateEvent}) {
        let activeMods = [];
        let onKeyDowns = [];
        let onKeyUps = [];

        ioHook.on("keydown", event => {
            let key = translateEvent(event);
            if (modKeys.indexOf(key) > -1 && activeMods.indexOf(key) == -1) {
                activeMods.push(key);
            }

            onKeyDowns.forEach((l) => l(key, activeMods));
        });
        ioHook.on("keyup", event => {
            let key = translateEvent(event);
            if (modKeys.indexOf(key) > -1) {
                Cols.remove1Mutate(activeMods, key);
            }
            onKeyUps.forEach((l) => l(key, activeMods));
        });
        // Register and start hook
        ioHook.start();

        return {
            onKeyDown(listener) {
                onKeyDowns.push(listener);

                return () => Cols.remove1Mutate(onKeyDowns, listener);
            },

            onKeyUp(listener) {
                onKeyUps.push(listener);

                return () => Cols.remove1Mutate(onKeyUps, listener);
            }

        };
    }
};

exports.KeyHook = KeyHook;