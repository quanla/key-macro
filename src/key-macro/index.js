const robot = require("robotjs");

var clipboard = nw.Clipboard.get();
// Register global desktop shortcut, which can work without focus.
nw.App.registerGlobalHotKey(new nw.Shortcut({
    key : "ctrl+X",
    active : function() {
        robot.keyTap("f6", "shift");
        robot.keyTap("c", "command");

        setTimeout(() => {
            let text = clipboard.get("text");
            robot.keyTap("home");
            robot.keyTap("right", "shift");
            robot.keyTap("right", "shift");
            robot.keyTap("right", "shift");

            if (text[0] == text[0].toLowerCase()) {
                robot.typeString("fbpe");
            } else {
                robot.typeString("Fbpe");
            }

            robot.keyTap("enter");
        }, 200);


        // clipboard.set(changeCase(clipboard.get("text"), CAMEL, DASH), 'text');
    },
    failed : function(msg) {
        // :(, fail to register the |key| or couldn't parse the |key|.
        console.log(msg);
    }
}));