const fs = require("fs");
const path = require("path");
var FileUtil = require("../common/utils/file-util.js").FileUtil;


// let dir = "/Users/quanle/Documents/Workon/Groupmatics/venue-editor/react";
let dir = "/Users/quanle/Documents/Workon/Groupmatics/venue-editor/react/venue-editor/editor-visual-panel/components/block/common";



FileUtil.recurse(dir, (dir, fileName) => {
    if (fileName.startsWith("section-")) {
        fs.renameSync(path.join(dir, fileName), path.join(dir, fileName.replace(/^section-/, "block-")));
    }
});